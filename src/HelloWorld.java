public class HelloWorld {

    /**
     * Qualquer programa Java tem que ter este método, é o ponto de entrada do seu sistema.
     *
     * @param args - Array com argumentos vindos da linha de comando
     */
    public static void main(String[] args) {

        // Welcome to Java!
        System.out.println("Hello world!");
    }
}
