package br.com.devlauten.curso.oop.javaoverview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Condicionais {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        int idade = 25;

        // IF
        if (idade >= 18) {
            System.out.println("Maior de idade");
        } else {
            System.out.println("Menor de idade");
        }

        reader.readLine();

        // ELSE IF
        if (idade < 18) {
            System.out.println("Menor de idade");
        } else if (idade == 18) {
            System.out.println("Virou maior de idade");
        } else {
            System.out.println("Ja é maior de idade");
        }

        reader.readLine();

        // SWITCH
        String nome = "Norberto";
        switch (nome) {
            case "Felipe":
                System.out.println("Felipe é um bonito nome!");
                break;
            case "Maria":
                System.out.println("Maria é uma pessoa muito gentil!");
                break;
            case "João":
                System.out.println("E ai João, beleza!?");
                break;

            default:
                System.out.println("Desculpa, não te conheço");
                break;
        }
    }
}
