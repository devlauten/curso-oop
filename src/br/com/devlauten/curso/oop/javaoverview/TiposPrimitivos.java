package br.com.devlauten.curso.oop.javaoverview;

public class TiposPrimitivos {

    public static void main(String[] args) {

        int idade = 18;
        float peso = 72.958f;
        double salario = 7509.87d;
        short horasTrabalhoDia = 8;

        String nome = "Felipe"; // <-- Não é primitivo, mas muito usado. :-)
    }
}
