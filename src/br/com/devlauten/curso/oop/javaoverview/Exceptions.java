package br.com.devlauten.curso.oop.javaoverview;

public class Exceptions {

    public static void main(String[] args) throws Exception {  // Notou alguma diferença?
        if (args == null || args.length == 0) {
            System.err.println("Informe o usuário como argumento do programa");
            System.exit(1);
        }

        String usuario = args[0];
        if (usuario.equals("Felipe")) {
            System.out.println("Acesso autorizado! Bem-vindo.");
        } else {
            throw new Exception("Usuário não está autorizado a usar o sistema");
        }
    }
}
