package br.com.devlauten.curso.oop.javaoverview;

public class HelloJava {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println("Hello Java!");
        } else {
            String name = args[0];
            System.out.println("Hello " + name + "!");
        }
    }
}
