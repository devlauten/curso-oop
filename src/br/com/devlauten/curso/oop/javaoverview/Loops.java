package br.com.devlauten.curso.oop.javaoverview;

public class Loops {

    public static void main(String[] args) {
        // FOR
        for (int i = 0; i < 10; i++) {
            System.out.println("Contador: " + i);
        }

        for (int i = 9; i >= 0; i--) {
            System.out.println("Contador regressivo: " + i);
        }

        // WHILE
        int count = 0;
        while(count < 10) {
            System.out.println("Countador While: " + count);
            count++;    // Alerta de loop infinito, não esqueça disso!
        }

        // DO WHILE
        do {
            System.out.println("Isso aqui será impresso???");
        } while (count < 10);

        // For..in será ensinado no modulo de collections, ok?
    }
}
