package br.com.devlauten.curso.oop.javaoverview;

public class Wrappers {

    public static void main(String[] args) {
        String nome = "Felipe";
        int idade = 30;
        double salario = 2500d;

        // Wrappers são "empacotadores de dados", veja o exemplo:
        Integer idadeWrapper = new Integer(idade);
        Double salarioWrapper = new Double(salario);

        // Mas por que se faz isso? Qual a diferença?

        System.out.println("Primitivo: " + idade);
        System.out.println("Wrapper..:" + idadeWrapper);
        System.out.println("Primitivo:" + salario);
        System.out.println("Wrapper..:" + salarioWrapper);

        // Dica, serialização. Será mais aprofundado depois ;)
    }
}
