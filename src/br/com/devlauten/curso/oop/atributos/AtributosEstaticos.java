package br.com.devlauten.curso.oop.atributos;

import java.util.Date;

public class AtributosEstaticos {

    static String versao = "v1.0.0";
    static Date hoje = new Date();
    static long inicio = System.currentTimeMillis();

    public static void main(String[] args) {
        System.out.println(versao);
        System.out.println(hoje);

        long fim = System.currentTimeMillis();
        System.out.println(fim - inicio);
    }
}
