package br.com.devlauten.curso.oop.atributos;

import java.util.Calendar;
import java.util.Date;

public class AtributosInstancia {

    public static void main(String[] args) {
        Calendar nascimentoFelipe = Calendar.getInstance();
        nascimentoFelipe.set(Calendar.DAY_OF_MONTH, 4);
        nascimentoFelipe.set(Calendar.MONTH, Calendar.NOVEMBER);
        nascimentoFelipe.set(Calendar.YEAR, 1987);


        Instancia instancia = new Instancia(nascimentoFelipe.getTime());

        System.out.println(instancia.getIdade());
    }

    static class Instancia {
        private Date dataNascimento;

        public Instancia(Date dataNascimento) {
            this.dataNascimento = dataNascimento;
        }

        public int getIdade() {
            Calendar nascimento = Calendar.getInstance();
            nascimento.setTime(this.dataNascimento);
            Calendar agora = Calendar.getInstance();
            agora.add(Calendar.YEAR, nascimento.get(Calendar.YEAR) * -1);

            return agora.get(Calendar.YEAR);
        }
    }
}
