# Curso OOP

Este repositório é destinado à turma do curso de Programação Orientada a Objetos (SENAC). Aula do dia 22/10/2018.
Aqui podem ser encontrados os exemplos e exercícios utilizados em sala de aula.

# Instrutor
Meu nome é Felipe Christian Lautenschlager. Sou um programador Java e trabalho com TI desde 2007. 
Nestes mais de 10 anos no mundo do desenvolvimento de aplicações Web, desenvolvi um interesse especial por código limpo, bem escrito, testável e bem estruturado.
Desde o início de minha carreira utilizo a OOP para o desenvolvimento de minhas atividades profissionais e projetos pessoais.
Tenho muito amor pelo código que eu escrevo (e espero que você tenha também). Embora não possa dizer que é propriedade minha, se você utilizou o Internet Banking pessoa jurídica do HSBC e recebeu alguma remessa de dinheiro do exterior, provavelmente já usou funcionalidades escritas por mim. =)
Tive essa bela surpresa de ser convidado pela minha amiga Rosilene a ministrar este mini-curso para vocês, e como todo marinheiro de primeira viagem, estou apreensivo. Mas espero que gostem do conteúdo e da didática!

Se quiserem manter contato, meu twitter é @DevLauten.

# Pré-requisitos

Para poder acompanhar os exemplos, tenha os seguintes itens instalados em seu computador:
	- Uma IDE de sua preferência (Eclipse ou Intellij são as minhas recomendações)
	- Cliente do GIT (Eu uso o Sourcetree da Atlassian)
	- Java JDK 8 (para rodar os programas)

# Instalação

Baixar os exemplos pelo GIT é muito fácil! =)
Apenas cole o seguinte comando no Terminal/Prompt de Comando:
`git clone https://devlauten@bitbucket.org/devlauten/curso-oop.git`

Se desejar, pode utilizar o seu cliente GIT para clonar o repositório (se você não gosta de telas pretas de Terminal, essa opção é para você!). Então entre apenas a URL do repositório:
https://devlauten@bitbucket.org/devlauten/curso-oop.git

# Comentários

Seus comentários e dúvidas serão muito bem-vindos! Caso existam em um momento posterior às aulas, por favor, registrem uma Issue no BitBucket. Vou ensinar a fazer isso, mas segue o link:
https://bitbucket.org/devlauten/curso-oop/issues?status=new&status=open

# Wiki
Gostou do que viu até aqui? Me ajudem a construir uma wiki para esse repositório, basta acessar o link e editar os documentos em: https://bitbucket.org/devlauten/curso-oop/wiki/Home

# Happy Coding! =)
